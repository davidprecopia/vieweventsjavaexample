package com.learning;

/**
 * You declare all the various Interfaces (contracts) the front end
 * will need to impl to interact with one another.
 */
public interface IViewContract {
    /**
     * In this particular example I do not need to add the type, however,
     * I wanted to combine how I have handled it with Ryan's example.
     * In his, this interface is top level and meant to be used across
     * multiple Logic classes.
     */
    interface Logic<T> {
        void onEvent(T event);
    }

    interface View {
    }

    interface ViewModel {
    }
}
