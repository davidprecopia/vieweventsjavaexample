package com.learning;

/**
 * {@link ViewEvents#getEvent()} returns once of it's {@link ViewEvents.Event}
 * {@link ViewEvents#getData()} return what that particular Event contains, if anything at all.
 *
 * The largest flaw with this is the lack of type safety.
 * It is possible to invoke {@link #getData()} when nothing has been assigned to it.
 * Moreover, you have to cast the Object to the expected type...
 */
public class ViewEvents {

    private final Event event;
    private final Object data;

    public ViewEvents(Event event, Object data) {
        this.event = event;
        this.data = data;
    }

    public Event getEvent() {
        return event;
    }

    public Object getData() {
        return data;
    }


    public enum Event {
        INIT,
        BURGER_SELECTED,
        DONE_CLICKED
    }
}