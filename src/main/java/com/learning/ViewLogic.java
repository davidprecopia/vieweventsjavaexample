package com.learning;

public class ViewLogic implements IViewContract.Logic<ViewEvents> {

    /**
     * {@link ViewEvents#getEvent()} returns once of it's {@link com.learning.ViewEvents.Event}
     * {@link ViewEvents#getData()} return what that particular Event contains, if anything at all.
     */
    @Override
    public void onEvent(ViewEvents event) {
        switch (event.getEvent()) {
            case INIT:
                // this will have to be cast since field type is Object.
                event.getData();
                break;
            case BURGER_SELECTED, DONE_CLICKED:
                break;
        }
    }
}
