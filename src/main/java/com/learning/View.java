package com.learning;

public class View implements IViewContract.View {

    private final IViewContract.Logic<ViewEvents> logic;

    //logic impl provided by Dependency Injection
    public View(IViewContract.Logic<ViewEvents> logic) {
        this.logic = logic;
        init();
    }

    //including data
    private void init() {
        logic.onEvent(new ViewEvents(ViewEvents.Event.INIT, this));
    }

    //not including data
    public void doneBtn() {
        logic.onEvent(new ViewEvents(ViewEvents.Event.DONE_CLICKED, null));
    }
}
